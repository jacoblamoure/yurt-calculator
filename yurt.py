import math

# made as a comparison to and with help from https://simplydifferently.org/Yurt_Notes?page=1

# params
khanaAngle = math.radians(90.)
lathLength = 8.*12.
yurtDiameter = 16.*12.
numberRafters = 32 # the maximum number of rafters.
                   # for actual build, can place them
                   # every 2nd or 3rd.
rafterLength = 8.*12.
roofAngle = math.radians(25.)


# output
wallHeight = lathLength/math.sqrt(2.)
circumfirence = math.pi*yurtDiameter
floorArea = math.pi*(yurtDiameter/2.)**2.
# sin(roofAngle) = (totalHeight-wallHeight)/rafterLength
# --> express this equation in terms of totalHeight
totalHeight = math.fabs(math.sin(roofAngle)*rafterLength) + wallHeight
roofHeight = totalHeight - wallHeight
# rafterLength**2. = ((yurtDiameter-wheelDiameter)/2.)**2. + (totalHeight-wallHeight)**2.
# pythagorean theorem --> invert this equation in terms of wheelDiameter
wheelDiameter = yurtDiameter - 2.*math.sqrt( rafterLength**2. - (roofHeight)**2. )
lathHoleSpacing = math.pi*yurtDiameter/(math.sqrt(2)*numberRafters)
horizontalKhanaSpacing = lathHoleSpacing*math.sqrt(2.)
numberHolesPerLath = math.ceil( (lathLength/lathHoleSpacing) - .5 )
volume = wallHeight*math.pi*(yurtDiameter/2.)**2. + math.pi*((yurtDiameter/2.)**2.)*roofHeight/3. # cylinder + cone

print("")
print("INPUTS:")
print("khanaAngle: "+str(math.degrees(khanaAngle)))
print("lathLength: "+str(lathLength))
print("yurtDiameter: "+str(yurtDiameter))
print("numberRafters: "+str(numberRafters))
print("rafterLength: "+str(rafterLength))
print("roofAngle: "+str(math.degrees(roofAngle)))

print("")
print("OUTPUTS:")
print("wallHeight: "+str(wallHeight))
print("circumfirence: "+str(circumfirence))
print("floorArea: "+str(floorArea))
print("totalHeight: "+str(totalHeight))
print("roofHeight: "+str(roofHeight))
print("wheelDiameter: "+str(wheelDiameter))
print("lathHoleSpacing: "+str(lathHoleSpacing))
print("horizontalKhanaSpacing: "+str(horizontalKhanaSpacing))
print("numberHolesPerLath: "+str(numberHolesPerLath))
print("volume: "+str(volume))
